# SWAP #

Serviço SWAP


## Instalar o NVM ##

> sudo apt-get update

> sudo apt-get install build-essential libssl-dev

> curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh

> bash install_nvm.sh

Para obter acesso à funcionalidade do nvm, você precisará sair e se logar novamente

## Instalar o NPM ##

> sudo apt-get install npm

## Instalar o Node ##

Listar as versões do Node disponíeis para download

> nvm ls-remote

Baixar a versão do Node escolhida

> nvm install 8.1.3

## Instalar o MongoDb ##

Tutorial para instalação: https://www.digitalocean.com/community/tutorials/como-instalar-o-mongodb-no-ubuntu-16-04-pt

## Intalar o Nodemon ##

Esse módulo é um utilitário que irá monitorar todas as alterações nos arquivos de sua aplicação e reiniciar automaticamente o servidor quando for necessário.

> npm install -g nodemon

## Executando a Aplicação ##

Certifique que o serviço do MongoDB está em execução. Caso não esteja, execute o comando:

> sudo service mongod start

Faça o clone do projeto. Depois entre pelo terminal na pasta do projeto e execute o comando:

> nodemon app.js

ou

> nodemon app

Pronto. O serviço está em execução.