var mongo = require('mongodb');

var connection = function() {
    var db = new mongo.Db(
        // Nome do banco
        'swap',
        // Objeto de conexão com o servidor
        new mongo.Server(
            // Endereço do servidor
            'localhost',
            // Porta de conexão
            27017,
            // Objeto com opções de configurações do servidor
            {}
        ),
        // Objeto com opções de configurações do banco
        {}
    );

    return db;
}

module.exports = function() {
    return connection;
}