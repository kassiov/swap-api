var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressSession = require('express-session');

var app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));

app.set('namespace', '/swap/api');

app.use(expressSession({
	/*
		Contém uma sequência de caracteres que será o segredo para assinar o cookie de sessão
		Serve como um ID para quando o usuário mandar um request, o express-session possa retornar
		os dados de acordo com o usuário.
	*/
	secret : 'hvieurvowieufbvlksjbvieurgvoslijdvbalirugoeiruvblasdkjbvleiru',
	/*
		Se for TRUE, a cada request do usuário essa sessão será salva no servidor
		Mesmo que esta não tenha sido modificada.
	*/
	resave : false,
	/*
		Se for TRUE, cria uma nova sessão toda vez que a mesma for modificada
	*/
	saveUninitialized : false
}));

consign()
	.include('./app/controllers')
	.then('./app/models')
	.then('./app/routes')
	.then('./config/database.js')
	.into(app);

module.exports = app;