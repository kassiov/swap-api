var crypto = require('crypto');

function UserDAO(connection) {
	this._connection = connection();
}

UserDAO.prototype.login = function(user, req, res, callback) {

	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('users', function(err, collection) {
			user.password = crypto.createHash('md5').update(user.password).digest('hex');
			var query = {
				email : user.email,
				password : user.password
			};
			collection.find(query).toArray(function(err, result) {
				mongoCLi.close();
				callback(result);
			});
		});
	});
}

UserDAO.prototype.searchByEmail = function(user, req, res, callback) {
	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('users', function(err, collection) {
			var query = {
				email : user.email
			};
			collection.find(query).toArray(function(err, result) {
				mongoCLi.close();
				callback(result);
			});
		});
	});
}

UserDAO.prototype.signup = function(user, req, res) {
	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('users', function(err, collection) {
			user.password = crypto.createHash('md5').update(user.password).digest('hex');
			collection.insert(user);
			mongoCLi.close();
		});
	});
};

UserDAO.prototype.search = function(req, res, callback) {
	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('users', function(err, collection) {
			var query = { name : { $regex : req.body.name, $options : 'i' } };
			collection.find(query).toArray(function(err, result) {
				mongoCLi.close();
				callback(result);
			});
		});
	});
};

module.exports = function() {
	return UserDAO;
}