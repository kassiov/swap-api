function KnowledgeDAO(connection) {
	this._connection = connection();
}

KnowledgeDAO.prototype.recommended = function(req, res, callback) {
	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('knowledges', function(err, collection) {
			var query = {};
			collection.find(query).toArray(function(err, result) {
				mongoCLi.close();
				callback(result, res);
			});
		});
	});
}

KnowledgeDAO.prototype.search = function(data, req, res, callback) {
	this._connection.open(function(err, mongoCLi) {
		mongoCLi.collection('knowledges', function(err, collection) {
			var query = { name : { $regex : data, $options : 'i' } };
			collection.find(query).toArray(function(err, result) {
				mongoCLi.close();
				callback(result, res);
			});
		});
	});
}

module.exports = function() {
	return KnowledgeDAO;
}