module.exports.login = function(application, req, res) {
	var data = req.body;
	console.log(data);
	var UserDAO = newConnection(application);

	var validateUser = function(result) {
		if(result.length > 0) {
			result[0].logged = true;
			res.send(result[0]);
		} else {
			result.name = null;
			result.email = null;
			result.password = null;
			result.logged = false;
			res.send(result);
		}
	}

	UserDAO.login(data, req, res, validateUser);
}

module.exports.logout = function(application, req, res) {
	res.send('logout');
}

module.exports.signup = function(application, req, res) {
	var data = req.body;
	console.log(data);
	var UserDAO = newConnection(application);

	var insertUser = function(result) {
		if(result.length < 1) {
			UserDAO = newConnection(application);
			UserDAO.signup(data, req, res);
			res.send({
				msg : 'Usuário cadastrado com sucesso',
				success: true
			});
		} else {
			res.send({
				msg : 'Usuário encontra-se cadastrado',
				success: false
			});
		}
	}

	UserDAO.searchByEmail(data, req, res, insertUser);
}

module.exports.search = function(application, req, res) {
	var UserDAO = newConnection(application);
	var searchUser = function(result) {
		for(var i = 0; i < result.length; i++) {
			result[i].logged = false;
		}
		res.send(result);
	}
	UserDAO.search(req, res, searchUser);
}

module.exports.delete = function(application, req, res) {
	res.send('delete');
}

module.exports.update = function(application, req, res) {
	res.send('update');
}

function newConnection(application) {
	var connection = application.config.database;
	return new application.app.models.UserDAO(connection);
}
