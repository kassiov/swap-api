var sendResult = function(result, res) {
	res.send(result);
}

module.exports.recommended = function(application, req, res) {
	var KnowledgeDAO = newConnection(application);
	KnowledgeDAO.recommended(req, res, sendResult);
}

module.exports.search = function(application, req, res) {
	var KnowledgeDAO = newConnection(application);
	var data = req.body;
	KnowledgeDAO.search(data.name, req, res, sendResult);
}

function newConnection(application) {
	var connection = application.config.database;
	return new application.app.models.KnowledgeDAO(connection);
}