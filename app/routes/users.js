module.exports = function(application) {

	var namespace = application.get('namespace') + '/user';

	application.post(namespace + '/login', function(req, res) {
		console.log('login');
		application.app.controllers.users.login(application, req, res);
	});

	application.get(namespace + '/logout', function(req, res) {
		console.log('logout');
		application.app.controllers.users.logout(application, req, res);
	});

	application.post(namespace + '/signup', function(req, res) {
		console.log('signup');
		application.app.controllers.users.signup(application, req, res);
	});

	application.post(namespace + '/search', function(req, res) {
		console.log('user/search');
		application.app.controllers.users.search(application, req, res);
	});

	application.delete(namespace + '/delete', function(req, res) {
		console.log('delete');
		application.app.controllers.users.delete(application, req, res);
	});

	application.put(namespace + '/update', function(req, res) {
		console.log('update');
		application.app.controllers.users.update(application, req, res);
	});
}