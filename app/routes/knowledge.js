module.exports = function(application) {
	
	var namespace = application.get('namespace') + '/knowledge';

	application.get(namespace + '/recommended', function(req, res) {
		console.log('recommended');
		application.app.controllers.knowledge.recommended(application, req, res);
	});

	application.post(namespace + '/search', function(req, res) {
		console.log('search');
		application.app.controllers.knowledge.search(application, req, res);
	});
}